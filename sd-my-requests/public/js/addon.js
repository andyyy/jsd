var groupBy = function(xs, keyGetter) {
    return xs.reduce(function(rv, x) {
      (rv[keyGetter(x)] = rv[keyGetter(x)] || []).push(x);
      return rv;
    }, {});
  };

$(document).ready(function () {
   // Require the request module
   AP.require(['request', 'messages'], function(request, messages) {
       // GET our Service Desk requests via the Jira Service Desk REST API
       request({
           url: '/rest/api/2/search',
           success: function (response) {
               // Parse the response JSON
               var json = JSON.parse(response);

               var reporters = groupBy(json.issues, it => it.fields.creator.accountId)

               var groupedReports = Object.values(reporters).sort((a, b) => a.length < b.length ? 1 : -1)

               // Did we get any requests back?
               if (groupedReports.length > 0) {
                   // Create a table with the resulting requests
                   $('<table>').addClass('aui').append(
                       $('<thead>').append(
                           $('<tr>').append(
                               $('<th>').text('Name'),
                               $('<th>').text('Count'),
                               $('<th>')
                           )
                       ),
                       $('<tbody>').append(
                           $.map(groupedReports, function (e) {
                               // Map each request to a HTML table row
                               return $('<tr>').append(
                                   $('<td>').text(e[0].fields.creator.displayName),
                                   $('<td>').text(e.length)
                               );
                           })
                       )
                   ).appendTo('#main-content');
               } else {
                   // Show a link to the Customer Portal
                   $('<div>').addClass('aui-message').append(
                       $('<p>').addClass('title').append(
                           $('<span>').addClass('aui-icon').addClass('icon-info'),
                           $('<strong>').text("It looks like you don't have any requests!")
                       ),
                       $('<p>').append(
                           $('<span>').text("Visit the "),
                           $('<a>').attr('href', '/servicedesk/customer/portals')
                                   .attr('target', '_blank')
                                   .text('Customer Portal'),
                           $('<span>').text(" to create some.")
                       )
                   ).appendTo('#main-content');
               }
           },
           error: function (err) {
               $('<div>').addClass('aui-message').addClass('aui-message-error').append(
                   $('<p>').addClass('title').append(
                       $('<span>').addClass('aui-icon').addClass('icon-error'),
                       $('<strong>').text('An error occurred!')
                   ),
                   $('<p>').text(err.status + ' ' + err.statusText)
               ).appendTo('#main-content');
           }
       });
   });
});
